---
title: "AWS DataSync,  FSx for Windows"
author: [Michał Stroiński]
date: "25/10/2021"
version: 1.0
copyrightyear: "2021"
---


**Content**

- [AWS DataSync, FSx for Windows](#aws-datasync-fsx-for-windows)
  - [Architecture](#architecture)
  - [On-Prem Simulated by Azure](#on-prem-simulated-by-azure)
    - [Create on-prem Environment](#create-on-prem-environment)
    - [Set up Hyper V](#set-up-hyper-v)
    - [Create on-prem Agent](#create-on-prem-agent)
  - [Cloud Location](#cloud-location)
    - [Create cloud VPC](#create-cloud-vpc)
    - [Create AWS Managed Microsoft AD](#create-aws-managed-microsoft-ad)
    - [Create vm-windows in the cloud](#create-vm-windows-in-the-cloud)    
    - [Create FSX for Windows Filesystem](#create-fsx-for-windows-filesystem)
    - [Mount FSx on vm-windows](#mount-fsx-on-vm-windows)    
  - [DataSync Agent](#datasync-agent)
  - [DataSync Locations](#datasync-locations)
    - [On-prem location: SMB](#on-prem-location-smb)
    - [Cloud location: FSX](#cloud-location-fsx)    
  - [DataSync Migrations](#datasync-migrations)   
    - [Migration form on-prem to FSx for Windows Fileserver](#migration-form-on-prem-to-fsx-for-windows-fileserver)

- [Documentation](#documentation)


# AWS DataSync,  FSx for Windows

## Architecture

![Arch](DataSync.png)

## On-Prem Simulated by Azure

### Create on-prem Environment

Create resources, which will simulate on-premise location.

* Create virtual network 10.20.0.0./24
* Create Virtual sever wich can be able support nested virtualization (Ds v3) (HyperV)
* Create Load Balancer SKU Basic
* Create Frontend IP (51.x.x.x.)
* Create Backend  pools where target is HyperV
* Create Load Balanicng rule mapping port 3389 to 13398
* Create Inbut NAT rule map front end port 80 to HyperV server

### Set up Hyper-V

* Install Hyper V service and tools  at server HyperV
```
Install-WindowsFeature -Name Hyper-V  -IncludeManagementTools -Restart
```

* Build Virtual switch  seu up network and NET NAt configuration to AWS agent
```
New-NetIPAddress -IPAddress 192.168.100.1 -PrefixLength 24 -InterfaceIndex 32

New-NetNat -Name "NATNetwork" -InternalIPInterfaceAddressPrefix 192.168.100.0/24

Add-NetNatStaticMapping -ExternalIPAddress "0.0.0.0/24" -ExternalPort 80 -Protocol TCP -InternalIPAddress 192.168.100.22 -InternalPort 80 -NatName NATNetwork
```
* Build VM wich emaluate FileServer 
* Set up some test shares

### Create on-prem Agent

* Download appilance from AWS pages 
* Build up vm from download vhd file
* setup ip 

![Arch](DataSyncIP.png)


## Cloud Location

### Create cloud VPC

Create three subnets. Enable auto-assign public IPv4 address. Associate subnets in Route Table. 
* vpc-onprem-subnet01 - az-a - 172.31.16.0/20
* vpc-onprem-subnet02 - az-b - 172.31.32.0/20
* vpc-onprem-subnet03 - az-c - 172.31.48.0/20

Create Internet Gateway and add default route in Route Table.

Create security group:
* Security group name: onprem-sg01
* VPC: vpc-onprem
* Inbound rules:
  * All traffic - onprem-sg01
  * HTTP, SMB - External  IP (for DataSync Agent activation, and comunication with File server)

 ### Create AWS Managed Microsoft AD

 * Create Directory service Microsoft AD

![Arch](DS.png)


### Create vm-windows in the cloud

* Create private route 53 zone from domain mscorp.test

![Arch](R53.png)

* Add DNS addres od Directory services to zone 

![Arch](DNS.png)


* Create IAM role for DomainJoin
 ( Attache policy AmazonSSMManagedInstanceCore, AmazonSSMDirectoryServiceAccess)

* Build VM where FSx file will be attached-with previously created  IAM role attached 

![Arch](EC2.png)

### Create FSX for Windows Filesystem

* Buid up FSX with connect to AWS Managed AD mscorp.test

### Mount FSx on vm-windows

* Run command at EC2
```
net use Z: \\amnfsxhx6aulsd.mscorp.test\share
```


## DataSync Agent

* Create AWS agent 
-  Hypervisor Hyer-V
-  Public endpoint
-  Activation key  public IPfrom Azure 51.x.x.x

![Arch](AgentR.png)



### On-prem location: SMB

* Create location for SMB file server located at Azure 

![Arch](SMB.png)

### Cloud location: FSX

* Create FSx location 

![Arch](FSX1.png)


## DataSync Migrations


### Migration form on-prem to FSx for Windows Fileserver

Run Task : resul files copied with Ownership and permissions to target location

![Arch](TASK1.png) 




